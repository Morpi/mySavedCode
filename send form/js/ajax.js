$(document).ready(function() {
	$("#contact__form").submit(function(e) {
    e.preventDefault();
    let form_data = $(this).serialize();
    $.ajax({
    type: 'POST',
    url: '../php/handler.php',
    data: form_data,
        success: function(data) {
        $("#contact__form").trigger("reset");
        $('.contact__modal').show(500)
        setTimeout(function() {
        	$('.contact__modal').hide(500)
        }, 2000)
      }
    });
  });	
})